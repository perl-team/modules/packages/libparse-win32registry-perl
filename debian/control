Source: libparse-win32registry-perl
Maintainer: Debian Perl Group <pkg-perl-maintainers@lists.alioth.debian.org>
Uploaders: Hilko Bengen <bengen@debian.org>
Section: perl
Testsuite: autopkgtest-pkg-perl
Priority: optional
Build-Depends: debhelper-compat (= 13)
Build-Depends-Indep: perl
Standards-Version: 4.6.1
Vcs-Browser: https://salsa.debian.org/perl-team/modules/packages/libparse-win32registry-perl
Vcs-Git: https://salsa.debian.org/perl-team/modules/packages/libparse-win32registry-perl.git
Homepage: https://metacpan.org/release/Parse-Win32Registry
Rules-Requires-Root: no

Package: libparse-win32registry-perl
Architecture: all
Depends: ${misc:Depends},
         ${perl:Depends}
Multi-Arch: foreign
Description: Perl module for parsing Windows registry files
 Parse::Win32Registry is a module for parsing Windows Registry files, allowing
 you to read the keys and values of a registry file without going through the
 Windows API.
 .
 It provides an object-oriented interface to the keys and values in a registry
 file. Registry files are structured as trees of keys, with each key
 containing further subkeys or values.
 .
 It supports both Windows NT registry files (Windows NT, 2000, XP, 2003,
 Vista) and Windows 95 registry files (Windows 95, 98, and Millennium
 Edition).
